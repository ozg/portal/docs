# Portalserver: Dokumentation

## Installation

```
mkdir portaldocs
cd portaldocs
python -m venv env
source ./env/bin/aktivate
pip install sphinx
git clone https://gitlab.com/ozg/portal/docs.git 
cd docs
make html
```
## Server für Dokumentation starten
```
cd portaldocs/docs/build
python http.server 
```
## Ansehen im Browser
```
http//localhost:8000
```

