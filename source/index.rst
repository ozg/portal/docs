.. https://www.kgst.de/documents/20181/34177/KGSt-Positionspapier_OZG_final.pdf/17e7c882-0983-e9c0-c56b-533677f42d7b
   https://k21media.de/matomo/matomo.php?idsite=1&rec=1

   https://www.heise.de/news/Akt-von-Cyberkriminalitaet-in-Frankreich-Patientendaten-aus-Laboren-im-Netz-5067272.html?wt_mc=rss.red.ho.ho.atom.beitrag.beitrag
   
===========================
 Onlinezugangsgesetz (OZG)
===========================

Mit Behörden kommunizieren ist nicht einfach. Ihre Mitarbeiter wissen
oft nicht, was in ihrem Umfeld, neben der eigentlichen Tätigkeit
noch passiert. Mit dieser kleinen Sammlung will ich ein wenig darüber
informieren, was ich so herausgefunden habe. Zentrales Thema ist
(vorerst) das OZG, weil ich es als Grundlage für Übungen genutzt habe
und jeden Behördenmitarbeiter den ich bis heute (31.10.2020) nach dem
OZG befragte, hat mit den Schultern gezuckt und den Kopf
geschüttelt. Also viel Raum für vermehrte Aufklärung und gesteigerte
Transparenz.

Portal-Konzeption
=================

.. toctree::
   :glob:
   :maxdepth: 1


   portal/anforderungen
   portal/datenfluss
   portal/fachverfahren/ubersicht
   prinzipien/dreiklickstrategie
   prinzipien/once-only
   ozg/umsetzung
   
Standards
=========

.. toctree::
   :glob:
   :maxdepth: 1

   fim-standards/index
   

Developer
=========
   
.. toctree::
   :glob:
   :maxdepth: 1

   develop/index
   develop/install/python

Linksammlung
============

* :ref:`linkliste`

  
