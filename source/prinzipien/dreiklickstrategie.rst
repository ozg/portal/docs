====================
Drei-Klick-Strategie
====================


.. index:: Drei-Klick-Strategie

Versuch/Protokoll vom <2020-11-01 So>
-------------------------------------

Es soll ja schnell und einfach sein, ist es aber (noch) nicht!
In dem Video wußte ich nach einigen Trainingsrunden schon, das 
der Eingabe des Ortes, die Auswahl über den eingeblendeten 
Link erfolgen muss!

Man wird nun zur Brandenburger Service-Seite weitergeleitet, 
leider weiß man dann immer noch nicht, wo man zum Gewerbe in 
Caputh Auskunft und Hilfe bekommt :-(



.. raw:: html

   <video controls width="450">
    <source src="../_static/media/behoerdenfinder.webm"
            type="video/webm">
    Dein Browser unterstützt keine eingebetten Videos.
   </video>
