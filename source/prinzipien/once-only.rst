=========
Once only
=========


.. index:: Once only

.. index:: eID

.. index:: Steuer-ID

Ein gern diskutiertes Prinzip, ist die Forderung, sich zentral und nur
einmal anmelden zu müssen, um dann alle (Dienst)-Leistungen der
Behörden nutzen zu können. Dieser zentralistische Ansatz ist mehr als
fragwürdig und alternative Vorschläge werden erst gar nicht bis selten
diskutiert. Während viele Fachverfahren vor lauter Komplexität selbst
von Fachleuten nur noch schwer zu handhaben sind, will man beim Bürger
mit der Einfachheit punkten. Einige Diskussionen zu diesem Thema
folgen als Linkliste.

- `https://www.heise.de/news/Vernetzte-Register-Kelber-warnt-vor-Vabanque-Spiel-mit-der-Steuer-ID-4946943.html <https://www.heise.de/news/Vernetzte-Register-Kelber-warnt-vor-Vabanque-Spiel-mit-der-Steuer-ID-4946943.html>`_

- `https://www.it-planungsrat.de/SharedDocs/Downloads/DE/Projekte/eID/Erfahrungsberichte_Buergerkonten.html?nn=6848604 <https://www.it-planungsrat.de/SharedDocs/Downloads/DE/Projekte/eID/Erfahrungsberichte_Buergerkonten.html?nn=6848604>`_

- `https://w3c.github.io/did-core/ <https://w3c.github.io/did-core/>`_

- `https://persosim.secunet.com/de/ <https://persosim.secunet.com/de/>`_
