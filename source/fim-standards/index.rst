=========================
FIM -- Leika -- Standards
=========================


Wofür steht der Laika-Katalog?
==============================

Der Leistungskatalog (LeiKa) ist ein Katalog von semantisch und
strukturell standardisierten Bezeichnungen einschließlich derer
Beschreibungen. Er stellt ein einheitliches, vollständiges und
umfassendes Verzeichnis der Verwaltungsleistungen über alle
Verwaltungsebenen hinweg dar. Hierzu wird ein föderales
Stammtextemanagement, auch mit Hilfe technischer Standards (XZufi),
etabliert. Der Betrieb des LeiKa sowie der zuständigen Geschäfts- und
Koordinierungsstelle GK-LeiKa.de (Umleitung zu https://fimportal.de/)
vom Land Sachsen-Anhalt gewährleistet. Der LeiKa ist eine
Basiskomponente, die in den Anwendungen Behördenfinder Deutschland
(BFD) und Behördennummer 115 verwendet wird. LeiKa bildet den Baustein
„Leistungen“ im Projekt Föderales Informationsmanagement (FIM).

Geschäfts- und Koordinierungsstelle Föderales Informationsmanagement
beim Ministerium der Finanzen des Landes Sachsen-Anhalt

Editharing 40 39106 Magdeburg

Föderale Informationsmanagement (FIM)
=====================================

Das Föderale Informationsmanagement (FIM) dient dazu, leicht
verständliche Bürgerinformationen, einheitliche Datenfelder für
Formularsysteme und standardisierte Prozessvorgaben für den
Verwaltungsvollzug bereitzustellen. Ziel ist es, den Übersetzungs- und
Implementierungsaufwand rechtlicher Vorgaben zu senken. Länder und
Kommunen sollen - bezogen auf die redaktionelle und organisatorische
Umsetzung eines Verwaltungsverfahrens - nicht mehr für sich alleine
agieren müssen. Stattdessen können sie auf qualitätsgesicherte
Vorarbeiten der nächsthöheren Verwaltungsebene zurückgreifen.


XML-Standards
=============

Begleitet werden die Prozesse und deren Umsetzung von diversen
Standards, die ich im XML-Kurs als Liste eingebunden habe.

- `Übersicht zu bekannten XML-Standards für die Verwaltung
  <https://inkubator.sudile.com/stories/kurse/xml/html/aab-anwendungsfaelle/xml-egov.html>`_

