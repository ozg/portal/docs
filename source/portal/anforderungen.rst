========================
Portale -- Anforderungen
========================

 
.. index:: Portale; Anforderungen

Über das OZG
------------

»OZG«, der Gesetzestext:  `Online Zugangs Gesetz <https://www.gesetze-im-internet.de/ozg/BJNR313800017.html>`_

Einige Zahlen:

.. table::

    +-----------------------+--------+
    | Verantwortungsbereich | Anzahl |
    +=======================+========+
    | Gesamt                |    575 |
    +-----------------------+--------+
    | davon...              | \      |
    +-----------------------+--------+
    | Bund                  |    115 |
    +-----------------------+--------+
    | Bund & Länder         |    370 |
    +-----------------------+--------+
    | Kommunen              |     95 |
    +-----------------------+--------+
    | Lebenslagen           |     35 |
    +-----------------------+--------+
    | Geschäftslagen        |     17 |
    +-----------------------+--------+

Portale
-------

Der Aufbau laut OZG:

.. image:: images/ozg.png

Grundsätzliche Forderungen sind:

- Transparenz

- bürgerzentrierte Lösungen

- medienbruchfreie Angebote

- barrierefreie Angebote

- Drei-Klick-Strategie d. h., die gesuchte Information
  bzw. der gesuchte Dienst soll innerhalb von drei Klicks
  aufgefunden werden.  [1]_

Nutzerkonten
------------

Nutzerkonten werden als allgemeine Lösung angestrebt und sollen
folgende Funktionen bieten:

- Identifizierung

- Authentifizierung

- rechtssichere elektronische Kommunikation

- einen Dokumentensafe zur Aufbewahrung elektronischer Bescheide

Pro und Kontra zu Nutzerkonten
------------------------------

- Behördenanfragen sind eher selten (Beispiel Ausweis/Pass) --
  Privat

- Als Gewerbetreibender eher öfter (Umsatzsteuermeldug,
  Statistiken,..., Ausschreibungen)

- Kontenanlage und Authentifizierung sind umständlich, oft an
  zusätzliche Hardware oder freigeschalteter Funktion im ePass
  gebunden.

  NFC-Pflicht!

- ein Vorgang hat nach einem längeren Zeitraum keinen Bezug zum
  aktuellen Stand. Beispiel Ausweis: wird nur alle 10 Jahre
  erneuert und beantragt

- Formulardaten lassen sich dezentral beim Benutzer speichern

Gegenentwurf
------------

Im Unterschied zum oben genannten zentralen Lösungsansatz, wird
hier die fallbasierte Abarbeitung von Fachverfahren analysiert und
ein Lösungsanatz zur Diskussion gestellt.

Über ein Online-Portal kann sich jeder Bürger, mit seinem Anliegen
an eine Behörde wenden. Dafür benötigt er nur zwei Informationen:

1. Wie heißt das Verfahren?

2. Welche Informationen benötigt der Bearbeiter und müssen vom
   Antragsteller zur Verfügung gestellt werden?

Zusätzlich solle er festlegen können, wie er weiter mit der Behörde
kommuniziert.

zu 1. Verfahren auswählen
~~~~~~~~~~~~~~~~~~~~~~~~~

Dieser Frage sollte über eine Auswahl- bzw. Suchstrategie im konkreten
Portal gelöst werden können. 

zu 2. Informationen zum Fachverfahren
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Noch vor dem Ausfüllen eines Formulars sollte der Antragsteller
darüber informiert werden, welche Angaben benötigt werden. Das
hilft einen unterbrechungsfreien Antragsvorgang über ein Formular
anzustoßen.

Beispiel: 


.. [1] `Architekturkonzept_eRechnung_final.pdf Seite 23 <https://www.finanzen.bremen.de/sixcms/media.php/13/Architekturkonzept_eRechnung_final.pdf>`_
