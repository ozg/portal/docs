=====================
Portale -- Datenfluss
=====================


.. index:: Datenfluss

Datenfluss Formulardaten
------------------------

Eine Idee, abweichend von schon implementierten Fachverfahren, die
eine zentrale Lösung verfolgen.


Der Kunde hat ein Formular ausgefüllt und sendet die Daten zum
Portalserver.  Weil es sich um sensible Daten handelt werden die Daten
mit dem Public Key einer Behörde verschlüsselt. Anschließend werden die
Daten auf einem Fallserver (F11N-Server) gespeichert. Für jeden Fall
wird ein Verzeichnis angelegt, in dem weitere Dokumente sowohl vom
Bürger als auch vom Bearbeiter abgelegt werden können.

.. image:: images/datenfluss.png

Datenfluss zwischen den Portalen
--------------------------------

Aus dem »Eckpunkte einer Datenstrategie der Bundesregierung« [2019]  [1]_ 

»Gesicherte Verbindungen zur Übermittlung von Daten innerhalb der
öffentlichen Verwaltung zur *ebenenübergreifenden Zusammenarbeit*
schaffen und auch weitere Maßnahmen der Datensicherheit prüfen.«


.. [1]    `https://www.bundesregierung.de/resource/blob/997532/1693626/e617eb58f3464ed13b8ded65c7d3d5a1/2019-11-18-pdf-datenstrategie-data.pdf?download=1 <https://www.bundesregierung.de/resource/blob/997532/1693626/e617eb58f3464ed13b8ded65c7d3d5a1/2019-11-18-pdf-datenstrategie-data.pdf?download=1>`_
