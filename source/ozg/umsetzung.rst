=================================================
Ein Katalysator für das Onlinezugangsgesetz (OZG)
=================================================

    :Author: Peter Koppatz

.. contents::

Ein Beitrag zum Stand der Umsetzung des »OZG« und zum internationalen
Frauentag.

.. image:: ./images/ozg.png

Warum braucht das »OZG« einen Katalysator?
------------------------------------------

Gesetze werden in den Medien immer wieder thematisiert und
beherrschen dann tagelang (oder wie im Fall Corona bereits
monatelang) die Meldungen und Diskussionen.
Das Kürzel »OZG« steht für das Onlinezugangsgesetz [1]_ . In dieser 
kleinen Untersuchung liegt der Schwerpunkt auf einer
Bestandsaufnahme, die Antworten auf die folgenden Fragen
geben soll:

1. Wird über das »OZG« berichtet?

2. Was erfährt der Bürger über die Umsetzung, Ziele,
   Fortschritte und Probleme der Umsetzung?

Bis zum Jahresende 2022 ist ein großer Schritt in Sachen
Digitalisierung und bei der Umsetzung des Gesetzes zu bewältigen.

Aus der Chemie kennt man das Verhalten, daß ein Stoff nur langsam
verbrennt; ein Katalysator [2]_  kann den Vorgang
beschleunigen. Bei der Umsetzung des »OZG« kann man diesen Vorgang
ebenfalls beobachten, nur welcher Katalysator hier helfen könnte,
ist noch nicht geklärt. Vielleicht hilft ja diese kleine
Wochenendstudie?

Dazu wurden 256 kreisfreie Städte und Kreise bzw. Landkreise
betrachtet. Die Analyse wurde durch die Eingabe der Zeichenfolge
»OZG« in die Suchmasken aller Webseiten realisiert.

Webseitengestaltung
~~~~~~~~~~~~~~~~~~~

Im Laufe der Untersuchung kamen einige Dinge zum Vorschein, die
ursprünglich nicht zur Fragestellung gehörten, dann aber als
zusätzliche Erkenntnisse erwähnt werden sollten.

Festzustellen bleibt, die Webauftritte gleichen sich in vielen
Punkten, weichen in einzelnen Details aber voneinander ab.

Aufgefallen sind wenige Webseiten, die dem Personenkult dienen,
weil Bürgermeister bzw. Landräte hier unbedingt ihre Grußbotschaften
verkünden müssen. Ob die Videobotschaft, wie bei der Bundeskanzlerin,
jede Woche aktualisiert wird, wurde nicht untersucht. Ich denke
diese Darstellungsform passt nicht in die heutige Zeit. Die Bürger 
kennen, wenn es sie interssiert, ihren Bürgermeister oder Landrat 
und der Tourist/Websitebesucher interessiert sich für die 
Sehenswürdigkeiten oder andere Themen.

Die zweite, als negativ zu bewertende Aktivität auf einigen
Webseiten ist die Zwangsinformation zur Coronakrise! Meine Damen und
Herren, sie wollen doch auf Ihrer Webseite allumfassend informieren.
Könnte es sein, daß mich die Coronasituation in Ihrem Landkreis
oder Ihrer Stadt gar nicht interessiert?

Machen Sie es doch wie die Stadt Neumünster. Ein zusätzlicher Link 
im Menü sollte den Corona-Interessierten genau soviel Spielraum 
geben wie demjenigen, der sich für das normale Leben in Ihrem 
Bereich interessiert! Gutes Beispiel gefällig:

.. image:: ./images/struktur-neumuenster.png

Hier ist das zentrale Element die Suchmaske über die alle Themen
aufrufbar sind. Andere Webauftritte haben einen extra Menüpunkt
für Corona integriert.

Auf eine weitere Unsitte möchte ich hinweisen. So schön die
Karusselbilder auf der Startseite auch sind, sie enthalten null
Information, manchmal nur ein schönes Motiv aus der Region (bei
Ausgrenzung aller anderen sehenswerten Objekte). 

Oft sind es sogar unsäglich langweilige Bilder oder haben Sie noch 
nie ein Bild vom Coronavirus gesehen -- sieht das Ding wirklich so
aus? 
Spielende Kinder, ja, aber das ist alles null Information, nimmt
Platz weg, verbraucht Bandbreite usw. Ein guter Kompromis ist es,
die Suchmaske und andere wichtige Links mit dem unvermeidlichen
Bild der Startseite zu verknüpftem.

Nun gut, wir klicken die Zwangsmeldung weg und können die Suche
starten, vorher muss aber auf vielen Seiten die Suchmaske gesucht
werden, nach dem Motto: Wir wissen was du suchst, warum wird die
Suchmaske versteckt (oft schwer zu orten)?
Aufgefallen ist mir, daß die Entwickler verlernt haben den Fokus auf
das Eingabefeld zu setzen, wenn durch einen Klick auf die Lupe das
Ansinnen eindeutig ausgedrückt worden ist. Warum muss ich warten,
bis das Eingabefeld der Suchmaske erschienen ist und dann nochmal
dort reinklicken?

Hat man die Suchmaske entdeckt, mal oben, links oder rechts, selten
unten, kann es losgehen: Eingabe »OZG« und schon zeigen sich die
Qualitäten des DMS/CMS (Dokumenten-, Contentmanagementsystem), also
die Basis aller Suchanfragen, die Dokumente und Daten, die wir
suchen.
Leider bekommt man als Ergebnis Dinge präsentiert, die wenig oder
nichts mit dem Suchbegriff zu tun haben, sind es die fehlenden
Inhalte, die Algorithmen zur Indizierung oder die Suchstrategien der
Software?

.. image:: ./images/kryptischer-link-nordsachsen.png

Im Allgemeinen war die Ausbeute sehr mager, das kann auch an dem
gewählten Suchbegriff gelegen haben. Aber hey, das Gesetz wurde 
am 14.08.2017 publiziert und innerhalb von drei Jahren hat es keine
Aufmerksamkeit erzeugt? Die Digitalisierungsinitiativen des
IT-Planungsrates gehen noch weiter zurück. Die Gründe kennen wir
nicht, eine Auswertung kann nur den ersten Status Quo feststellen,
was das Ziel dieser Befragung nebst Auswertung sein soll. Ich hoffe
damit die Diskussion etwas anzuregen.

Die in den DMS/CMS eingebauten Suchmaschinen sind nicht sonderlich
gut. Weiter unten finden Sie einen Vorschlag, wie die Ergebnisse
durch eine eigene Suchmaschine optimiert werden können.

\newpage

Methodik
~~~~~~~~

Was wurde nun gefunden? Die Methodik in Stichpunkten:

- Aufruf der Startseite (Wegklicken der Coockiefrage und falls
  notwendig, der Zwangsinformation über den Coronavirus)

- Aufruf der Suchfunktion

- Eingabe »OZG«

- Anzahl der Fundstellen ermitteln, die mit dem Kürzel »OZG« oder der Digitalisierung
  verknüpft waren

Ergebnis der Analyse
~~~~~~~~~~~~~~~~~~~~

Von 256 kreisfreien Städten und Kreisen bzw. Landkreisen haben 178
keinen Eintrag zum Suchbegriff erbracht. Einige lieferten wenigstens
einen Eintrag, wobei zu beobachten ist, daß es 2018/19
eine erste Häufung gegeben hat. In den Haushaltsplanungen 2020/21
war das Kürzel dann schon häufiger zu finden. Die Zeit drängt,
denn 2022 müssen die Vorgaben des Gesetzes umgesetzt sein! Nur wenige
Städte/Landkreise haben sich aktuell mit dem Thema
auseinandergesetzt. Die folgende Grafik zeigt, wie bisher
informiert worden ist. Erst die absoluten Zahlen, danach noch zwei
Grafiken.


.. table::

    +--------------+-----+
    | kein Treffer | 178 |
    +--------------+-----+
    | 1-2  Treffer |  59 |
    +--------------+-----+
    | 3-10 Treffer |  17 |
    +--------------+-----+
    | >11  Treffer |   2 |
    +--------------+-----+


- Wie unschwer zu erkennen ist, hat das »OZG« noch nicht alle
  Bereich der Verwaltungen erreicht bzw. sieht man sich nicht
  genötigt, darüber zu berichten.

- 1-2 Treffer sind in der Mehrzahl Seiten, die »OZG« als Verweis in
  einem Haushaltsplan oder anderen Dokumenten erwähnt haben.

- Die Gruppe der Kategorien 3-10 hat schon mehr Informationen im
  Angebot und oft weiterführende Erklärungen zum Thema.

Berlin fällt mit einer großen Anzahl positiv aus dem
Rahmen. Hier konnte ich natürlich nicht alle Verweise
kontrollieren, aber die ersten konnten eindeutig dem Thema
zugeordnet werden.

.. image:: ./images/ozg-balken.png

Allgemein ist das Ergebnis ernüchternd und es gibt viel Spielraum
für Verbesserungen. Dies bestätigt meine Erfahrung, daß nur wenige
Eingeweihte von dem Gesetzesvorhaben wissen und sich damit befassen.

.. image:: ./images/ozg-torte.png

Wie kann die Suche verbessert werden? Vergessen Sie die eingebauten
Suchfunktionen der Contentmanagemantsysteme (CMS) oder überarbeiten
Sie die Metadaten zu den Dokumenten! Und was noch? 
Eine andere Möglichkeit wäre die Installation einer eigenen Suchmaschine, 
über die Sie volle Kontrolle haben. »Yacy« [3]_  ist so eine
Lösung, deren Anschaffung, weil OpenSource, keine extra Kosten 
verursacht. 

.. image:: ./images/yacy-suchmaschine.png


Auch die Installation und der Betrieb sollte für den, der ein CMS
betreibt, im Rahmen des Möglichen liegen. Diese Suchmaschine kann
so konfiguriert werden, daß nur die Inhalte des eigenen DMS/CMS
indiziert werden. Die Suchmaske kann dann in das bestehende DMS/CMS
eingebaut werden und erlaubt die Optimierung der Suche,
beschleunigt sie sogar und Sie müssen keine externen
Serviceanbieter bezahlen. Ihre Bürger und Besucher werden es
danken, denn sie werden vor unliebsamen Tracking-Aktivitäten
geschützt.

.. image:: ./images/googletranslate-wunsiedel.png

\newpage

Zum Schluß
~~~~~~~~~~

Nun interessiert es Sie vielleicht, ob Sie auch zu den »Nullen«
gehören, nein nicht Sie persönlich, sondern ihr DMS/CMS, das auf eine
konkrete Frage keinen Verweis findet und Nonsens ausgibt?

Sie können die CSV-Datei mit den Ergebinssen herunterladen. Eine zweite
Datei dient dem Erfahrungsaustausch. Was mir beim Besuch der Seiten
aufgefallen ist, habe ich mit einem kurzen Kommentar versehen. Es
sind meine persönlichen ersten Eindrücke und manchmal konnte ich
ein wenig Ironie nicht unterdrücken, nehmen Sie es mir nicht
krumm und setzen Sie sich aktiv mit dem Feedback auseinander.
Eine weitere Spalte enthält einen Link zu dem ersten gefundenen
Dokument, und  kann als Referenz für die eigenen Infoseiten herangezogen 
werden. Ein gutes Beispiel sind Verweise auf die schon vorhandenen
Leistungen, wie es in Greifswald und auf anderen Webseiten bereits
praktiziert wird:

.. image:: ./images/buergerservice-greifswald.png

Weitere Untersuchung werden zeigen, wie gut die Serviceangebote
sind. Die positiven wie die weniger guten Angebote werde ich hier
ebenfalls dokumentieren. 
Die Dateien zur aktuellen Evaluation kann man über einen Link auf
der Webseite [4]_  online einsehen, betrachten und auch heruntergeladen. 
Ich nutze dafür einen kostenlosen Service, der sich »Binder« [5]_ 
nennt und Jupyter-Notebooks [6]_  in einer virtuellen Umgebung zur
Verfügung stellt. Bringen Sie etwas Geduld mit, wenn eine neue
virtuelle Maschine für Sie bereitgestellt wird. Wenn die virtuelle
Maschine im Browser den Ordner mit den Daten anzeigt, finden sie
die folgenden Dateien:

.. image:: ./images/ozg-jupyter.png

- **umfrage-ozg.ipynb** errechnet aus den Daten in umfrage-ozg.csv die
  gezeigten Diagramme

- **README.md** enthält einen allgemeinen Hinweis auf das Repository

- **requirements.txt** listet die verwendete Software auf

- **umfrage-ozg-kommentiert.csv** beinhaltet, wie der Name andeutet meine
  Kommentare zu Auffälligkeiten

- **umfrage-ozg.csv** enthält nur die Zahlen für die Auswertung

Ausblick
~~~~~~~~

Diese Bestandsaufnahme ist der Anfang einer kleine Serie und soll 
gute Umsetzungsstrategien liefern und die Hemmnisse einer raschen
Realisierung beobachten und benennen. 

Lizenz
~~~~~~

Es gelten auch die Regeln der Creative Commons Namensnennung 
4.0 International Lizenz.

.. image:: ./images/CC-BY.png

Sollten diese Lizenzen immer noch Unsicherheiten bezüglich der
Nutzung hervorrufen, erkläre ich hiermit einen Nichtangriffspakt 
und verzichte auf alle rechtlichen Mittel, da es nichts durchzusetzen
gibt.

Anhang
------

Download
~~~~~~~~


:download:`Download des Artikel als PDF <../_static/media/ozg-frage-2021.pdf>`

Auswertung über Binder
~~~~~~~~~~~~~~~~~~~~~~


.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/ozg%2Fumfragen/HEAD


.. [1] `https://www.gesetze-im-internet.de/ozg/OZG.pdf <https://www.gesetze-im-internet.de/ozg/OZG.pdf>`_

.. [2] `https://www.chemie.de/lexikon/Katalysator.html <https://www.chemie.de/lexikon/Katalysator.html>`_

.. [3] `https://www.yacy.de/ <https://www.yacy.de/>`_

.. [4] `https://inkubator.sudile.com/stories/kurse/ozg/html/ <https://inkubator.sudile.com/stories/kurse/ozg/html/>`_

.. [5] `https://mybinder.org/ <https://mybinder.org/>`_

.. [6] `https://jupyter.org/ <https://jupyter.org/>`_
