======================================
M -- wie »Modernes Verwaltungshandeln«
======================================




**»Modernes Verwaltungshandeln« -- Totes Pferd**
------------------------------------------------

Vorsicht Satire! Aber "Totes Pferd" läßt sich mit "Suchen &
Ersetzen"-Funktionen auf das aktuelle Projekt anpassen. Vielleicht
befreit der neue Blickwinkel aus einem Gefangnendilemma und 
es gelingt endlich abzusteigen...

.. index:: Modernes Verwaltungshandeln; Totes Pferd

.. index:: Totes Pferd; Modernes Verwaltungshandeln

*URL*: `Modernes Verwaltungshandeln -- Totes Pferd <http://www.roland-schaefer.de/totespferd.htm>`_
*letzter Zugriff*: 2021-02-28
