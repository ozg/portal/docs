=======================
S - wie Service-Portale
=======================


Welche Serviceportale können als Beispiele genannt werden:

**Hanau -- Serviceportal**
--------------------------

.. index:: Hanau; Serviceportal

.. index:: Serviceportal; Hanau

.. index:: ekom21; Hanau

.. index:: Hanau;  ekom21

*URL*: `Hanau -- Serviceportal <https://www.hanau-digital.de/bsp>`_
*letzter Zugriff*: 2021-03-19

**Anmerkung** \:\: Kooperation mit: IT-Dienstleister ekom21  

**Service -- Handbuch**
-----------------------

.. index:: Service; Handbuch

.. index:: Handbuch; Service

*URL*: `Service -- Handbuch <https://servicehandbuch.de>`_
*letzter Zugriff*: 2021-08-07

**Service -- Standards**
------------------------

.. index:: Service; Standards

.. index:: Standards; Service

*URL*: `Service -- Standards <https://servicehandbuch.de/servicestandard>`_
*letzter Zugriff*: 2021-08-07
