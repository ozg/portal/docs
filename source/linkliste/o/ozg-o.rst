============
O -- wie OZG
============




**Gesetz -- OZG**
-----------------

.. index:: Gesetz; OZG

.. index:: OZG; Gesetz

*URL*: `Gesetz -- OZG <https://www.gesetze-im-internet.de/ozg/BJNR313800017.html>`_
*letzter Zugriff*: 2021-02-27

**OpenSource -- eGovernment**
-----------------------------

.. index:: OpenSource; eGovernment

.. index:: eGovernment; OpenSource

*URL*: `OpenSource -- eGov <https://www.land.nrw/de/pressemitteilung/land-startet-pilotprojekt-fuer-open-source-software>`_
*letzter Zugriff*: 2021-03-23

**Anmerkung** \:\: Ein Pilotprojekt

Hoffenlich kann man aus dem Munix-Projekt was lernen.
