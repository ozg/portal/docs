==============
D -- wie Daten
==============




**Dashboard -- OZG**
--------------------

.. index:: Dashboard; OZG

.. index:: OZG; Dashboard

*URL*: `Dashboard -- OZG <https://www.onlinezugangsgesetz.de/Webs/OZG/DE/umsetzung/dashboard/ozg-dashboard/ozg-dashboard-node.html>`_
*letzter Zugriff*: 2021-03-03

Ein zentraler Überblick über den Fortschritt der OZG-Umsetzung. 

**Datenstrategie -- Bundesregierung**
-------------------------------------

.. index:: Datenstrategie; Bundesregierung

.. index:: Bundesregierung; Datenstrategie

*URL*: `Eckpunkte einer Datenstrategie der Bundesregierung <https://www.bundesregierung.de/resource/blob/997532/1693626/e617eb58f3464ed13b8ded65c7d3d5a1/2019-11-18-pdf-datenstrategie-data.pdf?download=1>`_
*letzter Zugriff*: 2021-02-27

**Dezentral Identifiers -- DID**
--------------------------------

.. index:: Dezentral Identifiers; DID

.. index:: DID; Dezentral Identifiers

.. index:: Identifiers; dezentral

*URL*: [[`https://w3c.github.io/did-core/ <https://w3c.github.io/did-core/>`_][DID -- Decentralized Identifiers]
*letzter Zugriff*: 2021-02-27
