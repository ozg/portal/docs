=================
G - wie Gutachten
=================




**Gutachten -- Digitalisierung**
--------------------------------

.. index:: Gutachten; Digitalisierung

.. index:: Digitalisierung; Gutachten

.. index:: Digitalisierung; Corona

.. index:: Corona; Digitalisierung

*URL*: `Gutachten -- Digitalisierung <https://www.bmwi.de/Redaktion/DE/Publikationen/Ministerium/Veroeffentlichung-Wissenschaftlicher-Beirat/gutachten-digitalisierung-in-deutschland.html>`_
*letzter Zugriff*: 2021-04-15
