.. _linkliste:

=========
Linkliste
=========
1. Irgendwo hab ich das schon mal gesehen oder gelesen, aber wo?

2. Diese FAQ ist eine kleine Merkliste...


.. |b| image:: ../_static/faq.png

.. |a| raw:: html

         <span>
           <img src='../_static/faq.png'
                alt='FAQ' />
          </span>

.. sidebar:: FAQ

	     | Wo hab ich das gesehen?
	     | Wo war das nur?
	     | |a|
	     
.. toctree::
   :maxdepth: 1
   :glob:

   a/*
   b/*
   d/*
   e/*
   f/*
   g/*
   i/*
   m/*
   n/*
   o/*
   s/*
