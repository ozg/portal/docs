====================
B - wie Brandenburng
====================




**Behörden -- Besuche**
-----------------------

.. index:: Behörden; Besuche

.. index:: Besuche; Behörden

.. index:: Behörden; Besuchermanagement

.. index:: Besuchermanagement; Behörden

*URL*: `Behörden -- Besuche <https://www.egovernment-computing.de/nutanix-germany-gmbh-c-282773/>`_
*letzter Zugriff*: 2021-03-18

**Brandenburg -- OZG (Infoseite)**
----------------------------------

.. index:: Brandenburg; OZG (Infoseite)

.. index:: OZG (Infoseite); Brandenburg

*URL*: `Brandenburg -- OZG (Infoseite) <https://ozg.brandenburg.de/ozg/de/>`_
*letzter Zugriff*: 2021-03-02

**OZG -- Infoseite (Bund)**
---------------------------

.. index:: OZG; Infoseite (Bund)

.. index:: Infoseite (Bund); OZG

.. index:: Bund (OZG); Infoseite

.. index:: Infoseite; Bund (OZG)

*URL*: `OZG -- Infoseite (Bund) <https://informationsplattform.ozg-umsetzung.de>`_
*letzter Zugriff*: 2021-03-02


**Anmerkung** \:\: Steuergelder von und für Bürger, warum dann das:

Auszug aus dem Impressum:

**Urheberrecht**

Das Copyright für Texte und Bilder liegt beim Bundesministerium des
Innern, für Bau und Heimat. Zur Verfügung gestellte Texte, Textteile,
Grafiken, Tabellen oder Bildmaterialien dürfen, soweit diese
urheberrechtlich geschützt sind, ohne vorherige Zustimmung des
Bundesministeriums nicht vervielfältigt, nicht verbreitet und nicht
ausgestellt werden. 
