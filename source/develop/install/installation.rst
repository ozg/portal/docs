=======================
Installation (Prototyp)
=======================

Entwicklungs- und Test-Umgebung einrichten
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: Environment

.. code:: bash


    mkdir <ozg-portal>
    cd <ozg-portal>
    python -m venv env

    # activate environment unix-like
    source ./env/bin/activate 
    # activate environment windows
    .\env\Scripts\activate.bat

Sourcecode für das Portal
-------------------------

.. index:: Portal; Sourcecode

Quellcode auf gitlab.com:

.. code:: bash


    cd <project-folder>
    git clone https://gitlab.com/ozg/portal/server.git

Portal setup und erster Start
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. index:: portal; Installation

.. code:: bash


    cd <ozg-portal>

    # activate unix-like
    source ./env/bin/activate 
    # activate on windows
    .\env\Scripts\activate.bat

    cd ozg
    pip install -e ".[testing]"

    pserve development.ini

Fachverfahren einbinden
~~~~~~~~~~~~~~~~~~~~~~~

Ein Fachverfahren »portaldemo« ist bereits im Portal integriert.
Weitere neue Fachverfahren lassen sich über einen
Cookiecutter-Template recht einfach an den Start bringen.

.. code:: bash


    # Einmalig cookiecutter installieren

    pip install cookiecutter

    # Template aufrufen

    cookiecutter https://gitlab.com/ozg/portal/coockiecutter_fachverfahren.git

    # Alternativ nach dem Download als zip-Datei

    cookiecutter /pfad/zum/download/der/zipdatei/coockiecutter_fachverfahren.zip
