=======================
Portalserver (Prototyp)
=======================

Prototyp
--------

Im Rahmen eines Semesters wurden 10 Fachverfahren als Prototyp
umgesetzt. Mehrere Studentengruppen haben den auf den nächsten
Seiten vorgestellten Portalserver verwendet und das Fachverfahren
integriert, was mit einem Cookcutter-Template recht einfach
möglich ist.

Ein Eindruck von der Software, die mit der Beschreibung auf den
Folgeseiten getestet werden kann, zeigt das folgende Bildschirmfoto.

.. image:: ../images/portalserver.webp
   :width: 500px 


.. index:: Python

Technologie
-----------

.. image:: images/python.png

Diese Dokumentation (Sphinx) und das Framework (Pyramid) für das
Webframework nutzen Python als Programmiersprache. Die Wahl für
die Programmiersprache »Python« wurde getroffen, weil sie für die
Implementierung eines Prototypen, für die Lehre und auch für den
produktiven Einsatz bestens geeignet ist.


.. index:: Portalserver; Prototyp
.. index:: Prototyp; Portalserver
