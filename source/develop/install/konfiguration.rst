=============
Konfiguration
=============


.. index:: Konfiguration; Daten

Wie werden aktuelle Daten eingebunden?
--------------------------------------

Die Formulardaten werden im ersten Schritt mit vordefinierten
Werten befüllt. Die Konfiguration befindet sich in der Datei:

»formdata_xxxx.py«

des jeweiligen Fachverfahren und kann an die örtlichen Gegebenheit
angepasst werden. Die Zeichenkette **xxxx** steht für die Bezeichnung des 
Fachverfahrens.

In der Konfiguration wird Python-Syntax verwendet!
Nach Änderungen kann die Syntax mit einem Testlauf überprüft
werden.

.. code:: bash


    pytest test_konfiguration.py

Datenfluss
----------

.. image:: ./images/datenfluss.png

.. index:: Link

.. index:: Route

.. index:: View

Zusammenspiel: Link-Route-View
------------------------------

Nachfolgend wird das Zusammenspiel von Konfiguration und Aufruf
einzelner Komponenten gezeigt:

Vom Aufruf eines Link (Request) bis zur Darstellung im Browser
(Response) sind viele Arbeitsschritte zu koordinieren. Wie das
Zusammenspiel auf Serverseite prinzipiell funktioniert, soll an
einem Beispiel erläutert werden.

.. image:: ./images/link-route-view-template.png

Kommentare zum Bild
~~~~~~~~~~~~~~~~~~~

- In der Funktion »includeme«                                           
  (__init__.py) werden die Routen           
  definiert.

- »/f11n/adt/index« ist ein Teil einer URL die im Menü       
  angeklickt wird.

- Der Server muss nun entscheiden, welcher             
  Programmteil auszuführen ist.

- Das wird an eindeutigen Namen          
  und/oder Parametern entschieden und gesteuert. Beide Festlegungen     
  sind in Zeile 4 definiert. Im gezeigten Fall ist es der Name der      
  Route »adt_1«.

- Was dann zu tun ist, steht in einer anderen Datei »adt_views.py«.

- Dort taucht der Name der Route wieder auf (Zeile 9). Gleichzeitig     
  wird festgelegt, welches Template zum Browser geschickt wird (        
  Zeile 10).

- Die Verarbeitung vor dem Abschicken findet in der          
  Funktion »adt_demo« statt. Im Beispiel passiert nicht viel,           
  lediglich eine Variable wird dem Template mitgegeben.

- Im Template »adt.jinja2« kann der Inhalt von Zeile 6 bis Zeile 19,    
  durch beliebige HTML-Konstrukte ersetzt werden.

- In den Zeilen 1-5 und abschließend Zeile 20 sind Anweisungen, die
  aus anderen Quellen gespeist werden, z.B. das einheitliche Layout
  oder das Menü.
