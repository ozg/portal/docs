==================
OZG und OpenSource
==================

Portal-Konzeption
=================

.. toctree::
   :glob:
   :maxdepth: 1


   opensource
   install/python
   install/installation
   install/konfiguration
